package com.mycompany.lab05;

public class Player {

    private String player = " X";
    
    public void setDefault(){
        this.player = " X";
    }
    public void switchPlayer(String player){
        if(player.equals(" X")){
            this.player = " O";
        }else{
            this.player = " X";
        }
    }
    
    public String getPlayer() {
        return player;
    }
}
